{-# LANGUAGE GeneralisedNewtypeDeriving, RankNTypes, LambdaCase #-}
module Control.Monad.Deferrable (
  DeferrableT,
  Deferrable,
  defer,
  fromFoldable,
  omega,
  bfs,
  toMaybe,
  runDeferrable,
  stretch,
  foldrS,
  foldrT,
  toList,
  toListOfN,
  toListWhile
 ) where

import Control.Applicative
import Control.Monad
import qualified Control.Monad.Deferrable.Generalized as G
import qualified Control.Monad.Fail as Fail
import Control.Monad.Trans.Class
import Data.Functor.Identity
import qualified Data.Sequence as Sq

-- | A monad transformer which adds nondeterminism with a
-- depth-first/breadth-first hybrid search strategy. Searching within each layer
-- is done depth-first; boundaries between layers are inserted with the 'defer'
-- action; and execution of each layer does not begin until the layer before it
-- is finished.
newtype DeferrableT m a = D (G.DeferrableT Sq.Seq m a) deriving (
  Functor, Applicative, Monad, Alternative, MonadPlus, MonadTrans,
  Fail.MonadFail,
  Monoid, Semigroup
 )
type Deferrable = DeferrableT Identity

-- | Create a level boundary.
defer :: DeferrableT m ()
defer = D (G.defer (flip (Sq.|>)))

-- | Makes a 'DeferrableT' value from a 'Foldable'
fromFoldable :: (Foldable t) => t a -> DeferrableT m a
fromFoldable = foldMap pure

-- | Returns the first element of the argument in the current layer; then each
-- subsequent element in a later layer.
omega :: (Foldable t) => t a -> DeferrableT m a
omega = foldr ((. (defer *>)) . (<|>) . pure) empty

-- | Breadth first search
bfs :: Monad m => (a -> m (Either [a] b)) -> a -> DeferrableT m b
bfs branch = go where
  go a = lift (branch a) >>= \case
    Left b -> defer >> fromFoldable b >>= go
    Right b -> return b

thisUncons :: Sq.Seq a -> Maybe (a, Sq.Seq a)
thisUncons s = case Sq.viewl s of
  Sq.EmptyL -> Nothing
  a Sq.:< r -> Just (a, r)

-- | Extract the first value, if it exists.
toMaybe :: Applicative m => DeferrableT m a -> m (Maybe a)
toMaybe = foldrT (\a _ -> pure (Just a)) (pure Nothing)

-- | Produces a value suitable for passing to the 'LogicT' constructor in the
-- logict package.
stretch ::
  DeferrableT m a -> (forall r . (a -> m r -> m r) -> m r -> m r)
stretch (D p) = G.stretch thisUncons Sq.empty p

-- | A generalization of 'foldrT' which allows for a state variable to be
-- passed along.
foldrS :: (a -> (s -> m r) -> s -> m r) -> (s -> m r) ->
  DeferrableT m a -> s -> m r
foldrS c z (D p) s0 = G.foldrS thisUncons Sq.empty c z p s0

-- | A variant of 'foldr' for consuming a 'DeferrableT'
foldrT :: (a -> m r -> m r) -> m r -> DeferrableT m a -> m r
foldrT c z (D p) = G.foldrT thisUncons Sq.empty c z p

-- | Converts a 'DeferrableT' to a list
toList :: Applicative m => DeferrableT m a -> m [a]
toList (D p) = G.toList thisUncons Sq.empty p

-- | Converts a 'Deferrable' to a list
runDeferrable :: Deferrable a -> [a]
runDeferrable = runIdentity . toList

-- | Converts a 'DeferrableT' to a list, stopping after n results
toListOfN :: Monad m => Int -> DeferrableT m a -> m [a]
toListOfN n0 (D p) = G.toListOfN thisUncons Sq.empty n0 p

-- | Converts a 'DeferrableT to a list, stopping at the first result which
-- doesn't satisfy the predicate.
toListWhile :: Monad m => (a -> m Bool) -> DeferrableT m a -> m [a]
toListWhile p (D a) = G.toListWhile thisUncons Sq.empty p a
