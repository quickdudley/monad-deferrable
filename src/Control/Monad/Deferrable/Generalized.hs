{-# LANGUAGE CPP #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RankNTypes #-}
module Control.Monad.Deferrable.Generalized (
  DeferrableT,
  defer,
  stretch,
  foldrS,
  foldrT,
  toList,
  toListOfN,
  toListWhile
 )  where
import Control.Applicative
import Control.Monad (MonadPlus(..), liftM2)
import qualified Control.Monad.Fail as Fail
import Control.Monad.Trans.Class

newtype R c s m r = R (s -> c (R c s m r -> R c s m r) -> m r)
-- | A generalized nondeterministic search monad with a search strategy
-- determined by the underlying container and the functions used for
-- manipulating it.
newtype DeferrableT c m a = DeferrableT
  (forall s r . (a -> R c s m r -> R c s m r) -> R c s m r -> R c s m r)

instance Functor (DeferrableT c m) where
  fmap f (DeferrableT c) = DeferrableT (c . (. f))

instance Applicative (DeferrableT c m) where
  pure a = DeferrableT ($ a)
  DeferrableT f <*> DeferrableT a = DeferrableT (f . (a .) . (.))
  DeferrableT a *> DeferrableT b = DeferrableT (a . const . b)
  DeferrableT a <* DeferrableT b = DeferrableT (a . ((b . const) .))

instance Monad (DeferrableT c m) where
  DeferrableT a >>= f = DeferrableT (a . flip (unD . f)) where
    unD (DeferrableT c) = c
  (>>) = (*>)
#if !MIN_VERSION_base(4,13,0)
  fail = Fail.fail
#endif

instance Alternative (DeferrableT c m) where
  empty = DeferrableT (\_ fk -> fk)
  DeferrableT a <|> DeferrableT b = DeferrableT (liftM2 (.) a b)

instance Fail.MonadFail (DeferrableT c m) where
  fail = const empty

instance MonadPlus (DeferrableT c m) where
  mzero = empty
  mplus = (<|>)

#if MIN_VERSION_base(4,9,0)
instance Semigroup (DeferrableT c m a) where
  (<>) = (<|>)
#endif

instance Monoid (DeferrableT c m a) where
  mempty = empty
#if !MIN_VERSION_base(4,9,0)
  mappend = (<|>)
#endif

instance MonadTrans (DeferrableT c) where
  lift a = DeferrableT (
    ((R . ((((a >>=) .) . flip) .) . flip . (unR .)) .) . flip
   ) where
    unR (R x) = x

-- | Defers the current line of computatio
defer :: (forall n . n -> c n -> c n) -> DeferrableT c m ()
defer df = DeferrableT ((R .) . flip (flip . ((.) .) . unR) . df . ($ ())) where
  unR (R x) = x

-- | Produces a value suitable for passing to the 'LogicT' constructor in the
-- logict package.
stretch :: (forall x . c x -> Maybe (x, c x)) -> (forall x . c x) ->
  DeferrableT c m a -> (forall r .  (a -> m r -> m r) -> m r -> m r)
stretch uncons e (DeferrableT a) = \c r -> let
  p = R (\s' bx -> case uncons bx of
    Nothing -> r
    Just (n, r') -> unR (n p) s' r'
   )
  j v (R n) = R (\s' bx -> c v (n s' bx))
  z (R x) = x () e
  in z (a j p)
 where
  unR (R x) = x

-- | A generalization of 'foldrT' which allows for a state variable to be
-- passed along.
foldrS ::
  (forall x . c x -> Maybe (x, c x)) ->
  (forall x . c x) ->
  (a -> (s -> m r) -> s -> m r) ->
  (s -> m r) ->
  DeferrableT c m a ->
  s ->
  m r
foldrS uncons e cons t (DeferrableT a) s = unR (a j p) s e
 where
  j v (R n) = R (flip (cons v . flip n))
  p = R (\s' bx -> case uncons bx of
    Nothing -> t s'
    Just (n, r) -> unR (n p) s' r
   )
  unR (R x) = x

-- | A specialization of 'foldr' for consuming a 'DeferrableT'; with additional
-- initial arguments for manipulating the underlying container.
foldrT ::
  (forall x . c x -> Maybe (x, c x)) ->
  (forall x . c x) ->
  (a -> m r -> m r) -> m r -> DeferrableT c m a -> m r
foldrT uncons e c z = flip (foldrS uncons e ((.) . c) (const z)) ()

-- | Converts a 'DeferrableT' to a list
toList ::
  Applicative m =>
  (forall x . c x -> Maybe (x, c x)) ->
  (forall x . c x) ->
  DeferrableT c m a -> m [a]
toList uncons e = foldrT uncons e (fmap . (:)) (pure [])

-- | Converts a 'DeferrableT' to a list, stopping after n results
toListOfN :: Applicative m =>
  (forall x . c x -> Maybe (x, c x)) ->
  (forall x . c x) ->
  Int -> DeferrableT c m a -> m [a]
toListOfN uncons e n0 p
  | n0 <= 0 = pure []
  | otherwise = foldrS uncons e (\a r n ->
    (a:) <$> (if n > 1 then r (n - 1) else pure [])
   ) (const $ pure []) p n0

-- | Converts a 'DeferrableT' to a list, stopping after n results
toListWhile :: Monad m =>
  (forall x . c x -> Maybe (x, c x)) ->
  (forall x . c x) ->
  (a -> m Bool) -> DeferrableT c m a -> m [a]
toListWhile uncons e p = foldrT uncons e (\a r -> p a >>= \case
  True -> (a:) <$> r
  False -> return []
 ) (return [])

