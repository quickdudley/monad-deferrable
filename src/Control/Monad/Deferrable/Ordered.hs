{-# LANGUAGE GeneralizedNewtypeDeriving, RankNTypes #-}
module Control.Monad.Deferrable.Ordered (
  DeferrableT,
  Deferrable,
  defer,
  stretch,
  foldrS,
  foldrT,
  toList,
  toListOfN,
  toListWhile,
  fromListAsc
 ) where

import Control.Applicative (Alternative(..))
import Control.Monad (MonadPlus)
import Control.Monad.Fail (MonadFail)
import Control.Monad.Trans.Class
import qualified Control.Monad.Deferrable.Generalized as G
import Data.Functor.Identity

-- | A monad transformer which adds nondeterminism with a
-- depth-first/prioritised hybrid search strategy. Searching within each layer
-- is done depth-first; boundaries between layers are inserted with the 'defer'
-- action; and execution of each layer does not begin until the layer before it
-- is finished.
newtype DeferrableT k m a = D (G.DeferrableT (SkewHeap k) m a) deriving (
  Functor, Applicative, Monad, Alternative, MonadPlus, MonadFail, MonadTrans,
  Semigroup, Monoid
 )
type Deferrable k a = DeferrableT k Identity a

data SkewHeap k a = Node k a (SkewHeap k a) (SkewHeap k a) | Tip

instance Ord k => Semigroup (SkewHeap k a) where
  Tip <> b = b
  b <> Tip = b
  a@(Node ka va la ra) <> b@(Node kb vb lb rb) = if ka <= kb
    then Node ka va (b <> ra) la
    else Node kb vb (a <> rb) lb

instance Ord k => Monoid (SkewHeap k a) where
  mempty = Tip

singleton :: k -> a -> SkewHeap k a
singleton = flip flip Tip . flip flip Tip . Node

-- | Create a level boundary.
defer :: Ord k => k -> DeferrableT k m ()
defer k = D (G.defer (flip (<>) . singleton k))

-- | Produces a value suitable for passing to the 'LogicT' constructor in the
-- logict package.
stretch :: Ord k =>
  DeferrableT k m a -> (forall r . (a -> m r -> m r) -> m r -> m r)
stretch (D p) = G.stretch pop Tip p

pop :: Ord k => SkewHeap k a -> Maybe (a, SkewHeap k a)
pop Tip = Nothing
pop (Node _ a l r) = Just (a, l <> r)

-- | A generalization of 'foldrT' which allows for a state variable to be
-- passed along.
foldrS :: Ord k =>
  (a -> (s -> m r) -> s -> m r) ->
  (s -> m r) ->
  DeferrableT k m a ->
  s ->
  m r
foldrS c r (D a) s0 = G.foldrS pop Tip c r a s0

-- | A variant of 'foldr' for consuming a 'DeferrableT'
foldrT :: Ord k =>
  (a -> m r -> m r) -> m r -> DeferrableT k m a -> m r
foldrT c r (D a) = G.foldrT pop Tip c r a

-- | Converts a 'DeferrableT' to a list
toList :: (Ord k, Applicative m) =>
  DeferrableT k m a -> m [a]
toList (D a) = G.toList pop Tip a

-- | Converts a 'DeferrableT' to a list, stopping after n results
toListOfN :: (Ord k, Applicative m) =>
  Int -> DeferrableT k m a -> m [a]
toListOfN n (D a) = G.toListOfN pop Tip n a

-- | Converts a 'DeferrableT to a list, stopping at the first result which
-- doesn't satisfy the predicate.
toListWhile :: (Ord k, Monad m) =>
  (a -> m Bool) -> DeferrableT k m a -> m [a]
toListWhile p (D a) = G.toListWhile pop Tip p a

-- | Creates a 'DeferrableT' from a possibly infinite list of priorities and
-- values. Each value is effectively deferred with its associated priority and
-- all previous priorities; which in the case where the priorities are in
-- ascending order is equivalent to each deferring with its own priority.
fromListAsc :: (Ord k) => [(k, a)] -> DeferrableT k m a
fromListAsc [] = empty
fromListAsc ((k,a):r) = defer k >> (pure a <|> fromListAsc r)

